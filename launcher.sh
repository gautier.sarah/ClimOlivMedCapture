#! /usr/bin/sh
module load singularity/3.5
module load jre/jre.8_x64
module load snakemake/5.13.0

snakemake -j 200 --cluster-config cluster.json --latency-wait 60 --cluster "/usr/bin/sbatch -p {cluster.queue} --mincpus={cluster.nCPUs} -J {cluster.jobname} --mem={cluster.memory}" 
