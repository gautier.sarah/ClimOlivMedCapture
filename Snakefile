#########################################################################################################################################
# PIPELINE
#########################################################################################################################################
#----------------------------------------------------------------------------------------------------------------------------------------
__author__ = "Gautier Sarah"
__license__ = "INRAE"
__date__ = "2020-07-17"
__maintainer__ = "Gautier Sarah"
__email__ = "gautier.sarah@inrae.fr"
__status__ = "Production"
#----------------------------------------------------------------------------------------------------------------------------------------

configfile: "config.yaml"

from snakemake.utils import min_version
from pathlib import Path

min_version("5.10.0")

#-----------------------------------------------------------------------------------------------------------------------------------------
# FUNCTIONS
#----------------------------------------------------------------------------------------------------------------------------------------

SAMPLE, = glob_wildcards(f"{config['Raw_Fastq']}/{{sample}}.R1.fastq.gz")

rule all:
    input:
        f'{config["output_prefix"]}/Variant_calling.vcf'
    message:
        "Variant calling done"

#Will only do the mapping step
rule mapping_only:
	input:
		bam=expand(f"{config['output_prefix']}/cleaned_alignments/{{sample}}.bam", sample=SAMPLE) if config["clean_alignment"]!=0 else expand(f"{config['output_prefix']}/rmduped_reads/{{sample}}.bam", sample=SAMPLE) if config["remove_duplicates"]!=0 else expand(f"{config['output_prefix']}/sorted_reads/{{sample}}.bam", sample=SAMPLE),
		bai=expand(f"{config['output_prefix']}/cleaned_alignments/{{sample}}.bam.bai", sample=SAMPLE) if config["clean_alignment"]!=0 else expand(f"{config['output_prefix']}/rmduped_reads/{{sample}}.bam.bai", sample=SAMPLE) if config["remove_duplicates"]!=0 else expand(f"{config['output_prefix']}/sorted_reads/{{sample}}.bam.bai", sample=SAMPLE),
	message:
		"Mapping done"

#WARNING : you !MUST! use the --nt option or all the resulting mapping files will be deleted. They are indeed marked as temp files.
rule raw_mapping_only:
	input:
		bam=expand(f'{config["output_prefix"]}/mapped_bam/{{sample}}.bam', sample=SAMPLE)
	message:
		"Reads have been mapped against the reference genome and the resulting alignements have been converted to BAM files"

#-----------------------------------------------------------------------------------------------------------------------------------------
# GENERAL MESSAGES
#----------------------------------------------------------------------------------------------------------------------------------------
# These messages will be print depending on the sucess or the failure of the pipeline

onsuccess:
	print("\nWorkflow finished. If the entire workflow has been runned, a file named Variant_calling.vcf has been created.\n")

onerror:
	print("An error occurred. You should read the readme file and see if everything have been setup the right way.")

#----------------------------------------------------------------------------------------------------------------------------------------
# WORKFLOW
#----------------------------------------------------------------------------------------------------------------------------------------
#Rename Create link to rename Raw_Fastq and have standardized name for R1 and R2

#Quality check of the raw fastq reads.
rule fastqc_raw:
	input:
		raw_R1=f"{config['Raw_Fastq']}/{{sample}}.R1.fastq.gz",
		raw_R2=f"{config['Raw_Fastq']}/{{sample}}.R2.fastq.gz"
	params:
		outdir=config['output_prefix']
	output:
		report_R1=f"{config['output_prefix']}/Raw_data/fastqc/{{sample}}.R1_fastqc.html",
                report_R2=f"{config['output_prefix']}/Raw_data/fastqc/{{sample}}.R2_fastqc.html"
	message:
		"Quality check of raw data with fastqc."
	shell:	
		"{config[fastqc][command]} {input.raw_R1} {input.raw_R2} -o {params.outdir}/Raw_data/fastqc/"

#Aggregation of the results from fastqc into a single report.
#rule multiqc_raw:
#
#

#The raw fastqc are trimmed, in order to increase quality of the reads to be aligned
rule trim_fastp:
	input:
		raw_R1=f"{config['Raw_Fastq']}/{{sample}}.R1.fastq.gz",
		raw_R2=f"{config['Raw_Fastq']}/{{sample}}.R2.fastq.gz"
	output:
		paired_R1=f"{config['output_prefix']}/Cleaned_data/{{sample}}_1_trimmed_Q30.fastq.gz",
		paired_R2=f"{config['output_prefix']}/Cleaned_data/{{sample}}_2_trimmed_Q30.fastq.gz",
	log:
		f"{config['output_prefix']}/LOGS/{{sample}}.TRIM.log"
	threads: 10
	message:
		"Trimming of raw data with trimmomatic, minimum quality 20."
	shell:
                "{config[fastp][command]} -i {input.raw_R1} -o {output.paired_R1} -I {input.raw_R2} -O {output.paired_R2} --qualified_quality_phred 30 --detect_adapter_for_pe --cut_tail --cut_tail_window_size 5 --cut_tail_mean_quality 30 "

#Quality check of trimmed data.
rule fastqc_trimmed:
	input:
		trim1=f"{config['output_prefix']}/Cleaned_data/{{sample}}_1_trimmed_Q30.fastq.gz",
		trim2=f"{config['output_prefix']}/Cleaned_data/{{sample}}_2_trimmed_Q30.fastq.gz"
	params:
		outdir=config['output_prefix']
	output:
		report_R1=f"{config['output_prefix']}/Cleaned_data/fastqc/{{sample}}_1_trimmed_Q30_fastqc.html",
                report_R2=f"{config['output_prefix']}/Cleaned_data/fastqc/{{sample}}_2_trimmed_Q30_fastqc.html"
	threads: 10
	message:
		"Quality check of trimmed data with fastqc."
	shell:	
		"{config[fastqc][command]} -t {threads} {input.trim1} {input.trim2} -o {params.outdir}/Cleaned_data/fastqc/;"

#Aggregation of the results from fastqc into a single report.
#rule multiqc_trimmed:
#
#

#The bwt is necessary for BWA to run, so we build it.
rule bwa_build_bwt:
	input:
		genome=expand("{genome}", genome=config["genome"])
	output:
		"{genome}.pac"
	log:
		"logs/index/bwa{genome}.log"
	message:
		"Building {output} index for {input}"
	shell:
		"{config[BWA][command]} index {input.genome} > {log}" 

#The genome also need to be indexed by samtools
rule samtools_faidx:
	input:
		genome=expand("{genome}", genome=config["genome"])
	output:
		"{genome}.fai"
	log:
		"logs/index/samtools_faidx_{genome}.log"
	message:
		"Indexing {input}..."
	shell:
		"{config[samtools][command]} faidx {input} > {log}" 

#Raw mapping, with conversion to Bam file with samtools. 
rule bwa_map:
	input:
		genome=expand("{genome}", genome=config["genome"]),
		bwt=expand("{genome}.pac", genome=config["genome"]),
		R1=f"{config['output_prefix']}/Cleaned_data/{{sample}}_1_trimmed_Q30.fastq.gz",
		R2=f"{config['output_prefix']}/Cleaned_data/{{sample}}_2_trimmed_Q30.fastq.gz",
        report_raw_R1=f"{config['output_prefix']}/Raw_data/fastqc/{{sample}}.R1_fastqc.html",
        report_raw_R2=f"{config['output_prefix']}/Raw_data/fastqc/{{sample}}.R2_fastqc.html",
        report_cleaned_R1=f"{config['output_prefix']}/Cleaned_data/fastqc/{{sample}}_1_trimmed_Q30_fastqc.html",
        report_cleaned_R2=f"{config['output_prefix']}/Cleaned_data/fastqc/{{sample}}_2_trimmed_Q30_fastqc.html"

	output:
		temp(f"{config['output_prefix']}/mapped_bam/{{sample}}.bam")
	log:
		"logs/mapping/{sample}.log"
	params:
		rg="@RG\\tID:{sample}\\tPL:ILLUMINA\\tSM:{sample}",
	threads: int(f"{config['BWA']['t']}")
	message:
		"Raw mapping with {input.R1} and {input.R2}..."
	shell:
		"{config[BWA][command]} mem -t {threads} {config[BWA][options]} -R '{params.rg}' {input.genome} {input.R1} {input.R2} 2> {log} | {config[samtools][command]} view -Sb - > {output} "

#Sorting reads with Samtools. Will be the last step before variant calling if rmdup and clean_alignment is set to false.		
rule samtools_sort:
	input:
		f"{config['output_prefix']}/mapped_bam/{{sample}}.bam"
	params:
		outdir=config['output_prefix']
	output:
		f"{config['output_prefix']}/sorted_reads/{{sample}}.bam"
	log:
		"logs/sorting/{{sample}}.log"
	threads: 10
	message:
		"Sorting {input}... with Samtools"
	shell:
		"{config[samtools][command]} sort -@ {threads} {input} -o {output} 2> {log}"

#Clean Alignment by keeping only primary alignment, properly paired and unique. Can be disabled in the config file
rule clean_alignment:
	input:
		bam=f"{config['output_prefix']}/rmduped_reads/{{sample}}.bam" if config["remove_duplicates"]!=0 else f"{config['output_prefix']}/sorted_reads/{{sample}}.bam"
	output:
		temp(f"{config['output_prefix']}/cleaned_alignments/{{sample}}.bam"),
	log:
		"logs/clean_mapping/{sample}.log"
	message:
		"Cleaning alignment for {input} "
	shell:
		"{config[samtools][command]} view -b -F 0x100 -f 0x02 -q 1 {input.bam} >{output} 2>{log}"

#Remove duplicates from sorted reads. Can be disabled in the config file
rule picard_rmdup:
	input:
		bam=f"{config['output_prefix']}/sorted_reads/{{sample}}.bam"
	output:
		temp(f"{config['output_prefix']}/rmduped_reads/{{sample}}.bam"),
	log:
		log1="logs/picard_rmdup/{sample}.log",
		log2="logs/picard_rmdup/{sample}.log2"
	params:
		tmp=f"{config['output_prefix']}/tmp"
	message:
		"Removing duplicates in {input}..."
	shell:
		"{config[software_reps][picard]} "
		"MarkDuplicates "
		"I={input} "
		"O={output} "
		"VALIDATION_STRINGENCY=SILENT "
		"MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 "
		"REMOVE_DUPLICATES=TRUE "
		"TMP_DIR={params.tmp} "
		"M={log.log1} "
		"2> {log.log2}"

#Index the bam file. Will index the final bam files, depending of the config file
rule samtools_index:
	input:
		"{sample}.bam" 
	output:
		"{sample}.bam.bai"
	message:
		"Indexing {input}..."
	priority:
		10
	shell:
		"{config[samtools][command]} index {input}"

#Create genome dictionary with Picard
rule picard_dict:
	input:
		genome=expand("{genome}", genome=config["genome"])
	output:
		expand("{genome_p}.dict", genome_p=config["genome_prefix"])
	message:
		"Creating dictionary for {input}..."
	shell:
		"{config[software_reps][picard]} "
		"CreateSequenceDictionary "
		"R={input} "
		"O={output}"

#The variant calling is done in 3 steps.

#VC - Step 1 : Create one gvcf file for each sample.
rule GATK_raw_calling:
	input:
		bam=f"{config['output_prefix']}/cleaned_alignments/{{sample}}.bam" if config["clean_alignment"]!=0 else f"{config['output_prefix']}/rmduped_reads/{{sample}}.bam" if config["remove_duplicates"]!=0 else f"{config['output_prefix']}/sorted_reads/{{sample}}.bam",
		bai=f"{config['output_prefix']}/cleaned_alignments/{{sample}}.bam.bai" if config["clean_alignment"]!=0 else f"{config['output_prefix']}/rmduped_reads/{{sample}}.bam.bai" if config["remove_duplicates"]!=0 else f"{config['output_prefix']}/sorted_reads/{{sample}}.bam.bai",
		genome=expand("{genome}", genome=config["genome"]),
		dictionary=expand("{genome_p}.dict", genome_p=config["genome_prefix"]),
		index_genome=expand("{genome}.fai", genome=config["genome"])
	output:
		f"{config['output_prefix']}/Raw_calling/{{sample}}.g.vcf",
	priority:
		10
	log:
		normal_log="logs/Raw_calling/logs/{sample}.log",
		error_log="logs/Raw_calling/error/{sample}.err"
	params:
		intervals="-L "+config[bed_file]+""  if config["bed_file"]!="" else ""    
	message:
		"Proceding raw GVCF calling on {input.bam}"
	shell:
		"java -Xmx4g -jar {config[software_reps][GATK]} "
		"HaplotypeCaller "
		"-ploidy {config[GATK][ploidy]} "
		"-ERC GVCF "
		"-R {input.genome} "
		"-I {input.bam} "
		"{params.intervals}"
		"{config[GATK][option]}"
		"-O {output} "
		"> {log.error_log} "
		"2> {log.normal_log}"

#VC - Step 1.bis : The following steps needs a list of the gvcf files to merge. This list is created here. The dictionary created at the begining 
rule GATK_file_list:
	input:
		expand(f"{config['output_prefix']}/Raw_calling/{{sample}}.g.vcf", sample=SAMPLE)
	output:
		f"{config['output_prefix']}/fileList.list"
	params:
		out_dir=f"{config['output_prefix']}"
	priority:
		10
	message:
		"Creating the list of input files for GATK"
	shell:
		"ls {params.out_dir}/Raw_calling/*.vcf > {output}"

rule GATK_chr_list:
	input:
		dict_file=rules.picard_dict.output
	output:
		chr_list=f"{config['output_prefix']}/chr.list"
	message:
		"Creating the sequence list for GenomicsDBImport"
	shell:
		"grep '@SQ' {input.dict_file} |cut -f2,3 | sed 's/SN:\([^\\t]*\)\\tLN:\([0-9]*\)/\\1:1-\\2/' >{output.chr_list}"

#VC - Step 2 : Merge all the GVCF files.
rule GATK_merge:
        input:
#		listing_number=expand(os.path.basename("{{fileList}}"), fileList=FILELIST),
                listing=ancient(f"{config['output_prefix']}/fileList.list"),
		genome=expand("{genome}", genome=config["genome"]),
		dictionary=expand("{genome_p}.dict", genome_p=config["genome_prefix"]),
		index_genome=expand("{genome}.fai", genome=config["genome"]),
		chr_lst=config[bed_file] if config["bed_file"]!="" else  rules.GATK_chr_list.output.chr_list           
        output:
                database=directory(f"{config['output_prefix']}/merged_raw_calling.db")
        params:
                tmp=f"{config['output_prefix']}/tmp"
        priority:
                10
        log:
                normal_log=f"logs/Merging_GVCF/Merging_GVCF.log",
                error_log=f"logs/Merging_GVCF/Merging_GVCF.err"
        params:
                intervals="-L "+config[bed_file]+""  if config["bed_file"]!="" else "-L {input.chr_lst}",
        
        threads: int(f"{config['GATK']['t']}")
        message:
                "Merging GVCFs files..."
        shell:
                "mkdir -p {params.tmp} && "
                "java -Xmx120g -Djava.io.tmpdir={params.tmp} "
                "-jar {config[software_reps][GATK]} "
                "GenomicsDBImport "
                "--overwrite-existing-genomicsdb-workspace True "
                "-R {input.genome} "
#		"--variant {config['output_prefix']}/{input.listing} "
                "--variant {input.listing} "
                "--batch-size 50 "
                "--reader-threads {threads} "
                "-L {input.chr_lst} "
                "--merge-input-intervals "
                "--genomicsdb-workspace-path {output.database} "
                "--tmp-dir {params.tmp} "
                "> {log.error_log} "
                "2> {log.normal_log}"

#VC - Step 3 : Convert the resulting gvcf file to a vcf file. End of the variant calling.
rule GATK_final_calling:
	input:
		database=f"{config['output_prefix']}/merged_raw_calling.db",
		genome=expand("{genome}", genome=config["genome"]),
		dictionary=expand("{genome_p}.dict", genome_p=config["genome_prefix"]),
		index_genome=expand("{genome}.fai", genome=config["genome"])
	output:
		f"{config['output_prefix']}/Variant_calling.vcf"
	priority:
		10
	params:
		tmp=f"{config['output_prefix']}/tmp"
	log:
		normal_log="logs/Final_calling/Variant_calling.log",
		error_log="logs/Final_calling/error/Variant_calling.err"
	message:
		"Proceding variant calling..."
	shell:
		"java -Xmx4g -Djava.io.tmpdir={params.tmp} "
		"-jar {config[software_reps][GATK]} "
		"GenotypeGVCFs "
		"-R {input.genome} "
		"-V gendb://{input.database} "
		"-O {output} "
		"2> {log.error_log}"
		"> {log.normal_log}"


#Created by Alexandre Soriano - 2017 - alex.soriano@laposte.net
#Adapted by Gautier Sarah - 2020 - gautier.sarah@inrae.fr
