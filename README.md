# ClimOlivMedCapture


## Name
ClimOlivMed Capture workflow.

## Description
Here is the snakemake workflow that was used in climOlivMed project to go from raw Illumina reads to a raw VCF

<p align="center">
<img src="dag.svg" width="50%">
</p>


## Author
Gautier Sarah (INRAE)

